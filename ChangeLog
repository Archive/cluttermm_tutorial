2010-09-21  Chris Kühl  <chrisk@openismus.com>

	Fixed opacity issue in custom_actor.

	* examples/custom_actor/triangle_actor.cc: Changed use of
	get_opacity() to color_.get_alpha() in pick_vfunc.
2010-09-20  Chris Kühl  <chrisk@openismus.com>

	Fixed issue where custom_actor example was not appearing.

	* examples/custom_actor/triangle_actor.cc: Changed Cogl::Fixed
	array to float array.

2010-09-17  Chris Kühl  <chrisk@openismus.com>

	Fixed reference count and scope issues in behaviour example.

	* examples/behaviour/main.cc: Changed return value of on_alpha
	and freed code from block.

2010-09-13  Murray Cumming  <murrayc@murrayc.com>

	Finish fixing the build with --enable-warnings=fatal

	* examples/timeline/main.cc: Comment out another unused parameter.
  I could get this far after fixing a GTKMM-GDKMM typo in gtkmm 2.22.

2010-09-13  Murray Cumming  <murrayc@murrayc.com>

	Partly fix the build with --enable-warnings=fatal

	* configure.ac: Add  -Wno-long-long because the clutter headers trigger it.
	* examples/custom_container/examplebox.cc:
	* examples/full_example/main.cc:
	* examples/score/main.cc: Comment out unused parameters and use
  RefPtr::reset() instead of deprecated RefPtr::clear().

2010-09-02  Murray Cumming  <murrayc@murrayc.com>

	Fix some runtime errors.

	* examples/full_example/main.cc: add_image_actors(): Avoid giving an
  angle to ClutterBehaviourEllipse that is > 360, because that is not
  allowed. This probably doesn't work properly now.
  add_image_actors(): Avoid trying to load the images/README file as an image.

2010-04-06  Murray Cumming  <murrayc@murrayc.com>

    Mention Clutter::Text instead of the Clutter::Label and Clutter::Entry.

	* docs/tutorial/cluttermm-tut.xml: Actors: Mention Clutter::Text instead of
    the old Clutter::Label and Clutter::Entry actors. This is based on the
    same change in clutter-tutorial.

2010-04-05  Murray Cumming  <murrayc@murrayc.com>

  Update links and mention clutterm--0.10.

	* docs/tutorial/cluttermm-tut.xml: Update the reference links to use
	library.gnome.org and mention cluttermm-0.10 instead of cluttermm-0.9.

2010-04-05  Murray Cumming  <murrayc@murrayc.com>

  Update for the latest cluttermm and clutter-gtkmm.

  * configure.ac: Check for clutter-gtkmm-1.0, not clutter-gtkmm-0.9.
	* examples/*/*.[h|cc]: Update for the latest API, based on the changes in
	the corresponding clutter-tutorial examples. This at least builds now
	though I have no idea if the code is correct. We should check more carefully
	against the recent changes in the clutter tutorial.
  * docs/tutorial/cluttermm-tut.xml: Removed the multiline text editing
  appendix, because regular cluttermm can now do this.
	* docs/tutorial/figures/multiline_text_entry.png:
	* examples/multiline_text_entry/: Removed these parts of the example.

2009-03-16  Siavash Safi  <siavash@siavashs.org>

	* trunk/INSTALL: Autoupdated by autoconf
	* trunk/Makefile.am: Disabled effects example (should be updated to use
	Clutter::Animation when it's wrapped)
	* trunk/examples/actor/main.cc:
	* trunk/examples/actor_group/main.cc:
	* trunk/examples/actor_transformations/main.cc:
	* trunk/examples/custom_actor/triangle_actor.cc:
	* trunk/examples/full_example/main.cc: Updated to use new API

2009-02-17  Daniel Elstner  <danielk@openismus.com>

	* autogen.sh: Add minimal variant of this common shell script
	which simply runs autoreconf and then configure.

2009-01-07  Daniel Elstner  <danielk@openismus.com>

	* docs/tutorial/README: Document "make html".

2009-01-07  Daniel Elstner  <danielk@openismus.com>

	* examples/full_example/main.cc (Example::on_texture_button_press):
	Fix typo.

2009-01-07  Daniel Elstner  <danielk@openismus.com>

	* examples/full_example/main.cc (Example::rotate_item_to_front):
	Disable a piece of code that calculates a value which is not used.

2009-01-07  Daniel Elstner  <danielk@openismus.com>

	* examples/full_example/main.cc (Example::Example): Simplify
	expression.

2009-01-07  Daniel Elstner  <danielk@openismus.com>

	* examples/behaviour/main.cc (main): Correct a method name mentioned
	in a comment.

2009-01-07  Daniel Elstner  <danielk@openismus.com>

	* Makefile.am: Minor cleanup.
	(post-html): Prefix the examples directory with $(srcdir) because
	the sources do not live in the build tree in a VPATH setup.  Also
	make use of the -C option of rsync to omit build garbage from the
	upload.

2009-01-06  Murray Cumming  <murrayc@murrayc.com>

	* docs/tutorial/cluttermm-tut.xml: Introduction: cluttermm: Correct a
	typo and mention that exceptions are optional (at glibmm configure time)
	for embedded developers who are scared of them.

2009-01-06  Murray Cumming  <murrayc@murrayc.com>

	* docs/tutorial/cluttermm-tut.xml: Correct the base URLs for the
	(not ideal) reference documentation for cluttermm and clutter-gtkmm.
	* examples/full_example/main.cc: Replaced some g_print()s with
	std::cout.

2009-01-06  Murray Cumming  <murrayc@murrayc.com>

	* examples/: Mostly whitespace changes to be more like the gtkmm
	examples.

2009-01-06  Murray Cumming  <murrayc@murrayc.com>

	* COPYING:
	* INSTALL: Added these to svn.
	* Makefile.am: Correct the upload, which must be a little more complex
	for the new non-recursive build structure.

2009-01-06  Daniel Elstner  <danielk@openismus.com>

	* docs/tutorial/insert_example_code.pl: Look for files with ".cc"
	extension rather than ".c".  Also be more lenient with regards to
	the capitalization of "Source code".

2009-01-06  Daniel Elstner  <danielk@openismus.com>

	* Makefile.am (cluttermm_docdir): Rename from clutter_docdir and
	change the path to point to the cluttermm-0.9 documentation instead
	of the clutter one.

2009-01-06  Daniel Elstner  <danielk@openismus.com>

	* docs/tutorial/cluttermm-tut.xml: Rework the Clutter tutorial for
	the Cluttermm API.

2009-01-06  Daniel Elstner  <danielk@openismus.com>

	* Makefile.am (bin_PROGRAMS): Rename to noinst_PROGRAMS, or every
	example executable will be installed as $(bindir)/example -- the
	last one wins.  Do we want to install the example binaries, and if
	so, where?
	({,un}install-tutorial): Depend on docs/tutorial/html/index.html
	rather than the phony target html, because the rule derives the
	location of the html build directory from the first dependency.

2009-01-06  Murray Cumming  <murrayc@murrayc.com>

	* Makefile.am: Correct the upload URL.

2009-01-06  Daniel Elstner  <danielk@openismus.com>

	* docs/index.html: Copy file from Clutter tutorial.
	* docs/tutorial/README: ditto,
	* docs/tutorial/cluttermm-tut.xml: ditto,
	* docs/tutorial/docbook_phpwebnotes.xsl: ditto,
	* docs/tutorial/insert_example_code.pl: ditto,
	* docs/tutorial/style.css: ditto.
	* Makefile.am: Integrate the documentation build infrastructure
	from the Clutter tutorial into the non-recursive build system.

2008-12-31  Daniel Elstner  <danielk@openismus.com>

	* examples/multiline_text_entry/{multiline_entry.{cc,h},main.cc}:
	New example implementing a multi-line text entry actor.  Converted
	from the C Clutter tutorial example of the same name.
	* Makefile.am (bin_PROGRAMS): Add
	examples/multiline_text_entry/example.

2008-12-30  Daniel Elstner  <danielk@openismus.com>

	* examples/scrolling/{scrollingcontainer.{cc,h},main.cc}: New
	example demonstrating a custom container which scrolls its content.
	Converted from the C Clutter tutorial example of the same name.
	* Makefile.am (bin_PROGRAMS): Add examples/scrolling/example.
