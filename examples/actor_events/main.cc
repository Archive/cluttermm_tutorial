/*
 * Copyright (c) 2011-2016 cluttermm developers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <cluttermm.h>
#include <iostream>

//Define some functions for the signal handlers to call: 
bool on_my_button_press(Clutter::ButtonEvent* event, const Glib::RefPtr<Clutter::Group>& g)
{
  g->set_rotation(Clutter::X_AXIS, 330, 0, 0, 0);
  std::cout << "Pressed button at "<< event->x << "/ " << event->y << std::endl;
  return true;
}

bool on_my_button_release(Clutter::ButtonEvent* event, const Glib::RefPtr<Clutter::Group>& g)
{
  g->set_rotation(Clutter::X_AXIS, 30, 0, 0, 0);
  std::cout << "released button at " << event->x << "/ " << event->y << std::endl;
  return true;
}

bool on_my_mouse_enter(Clutter::CrossingEvent* event, const Glib::RefPtr<Clutter::Group>& /* g */)
{
  std::cout << "mouse entered at " << event->x << "/ " << event->y << std::endl;
  return true;
}

bool on_my_mouse_leave(Clutter::CrossingEvent* event, const Glib::RefPtr<Clutter::Group>& /* g */)
{
  std::cout << "mouse left from " << event->x << "/ " << event->y << std::endl;
  return true;
}

int main(int argc, char** argv)
{
  Clutter::init(&argc, &argv);

  // Get the stage and set its size and color:
  const Glib::RefPtr<Clutter::Stage> stage = Clutter::Stage::get_default();
  stage->set_size(200, 300);
  stage->set_color(Clutter::Color(0, 0, 0, 0xFF)); // black

  // Add a group to the stage:
  const Glib::RefPtr<Clutter::Group> group = Clutter::Group::create();
  group->set_position(40, 40);
  stage->add_actor(group);
  group->show();
  const Clutter::Color actor_color(0xFF, 0xFF, 0xFF, 0x99);

  // Add a rectangle to the group:
  const Glib::RefPtr<Clutter::Rectangle>
    rect = Clutter::Rectangle::create(actor_color);
  rect->set_size(50, 50);
  rect->set_position(0, 0);
  group->add_actor(rect);
  rect->show();

  // Show the stage:
  stage->show();
  rect->set_reactive();
  group->set_reactive();

  //connect some signal handlers:
  stage->signal_button_press_event().connect(
    sigc::bind(sigc::ptr_fun(&on_my_button_press), group));
  rect->signal_button_release_event().connect(
    sigc::bind(sigc::ptr_fun(&on_my_button_release), group));
  group->signal_enter_event().connect(
    sigc::bind(sigc::ptr_fun(&on_my_mouse_enter), group));
  group->signal_leave_event().connect(
    sigc::bind(sigc::ptr_fun(&on_my_mouse_leave), group));

  // Start the main loop, so we can respond to events:
  Clutter::main();
  return 0;
}

