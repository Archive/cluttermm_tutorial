#! /bin/sh
# Copyright (c) 2009  Openismus GmbH  <http://www.openismus.com/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#! /bin/sh -e
test -n "$srcdir" || srcdir=`dirname "$0"`
test -n "$srcdir" || srcdir=.

# cluttermm-tutorial depends on the mm-common module. Stop if it's not
# installed.
# Also copy files from mm-common to the build/ directory so that things like
# auto-generating the ChangeLog from the git commit messages on 'make dist' is
# possible.
mm-common-prepare --copy --force "$srcdir"

(
  cd "$srcdir" &&
  autoreconf --force --install --verbose
) || exit
test -n "$NOCONFIGURE" || "$srcdir/configure" "$@"
